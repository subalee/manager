import React from 'react';
import { View, Text } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers';
import firebase from 'firebase';
import LoginForm from './components/LoginForm';

class Core extends React.Component {

    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyBuL6Gp7WFgy_Chm96zSckbfP50ibpyS7o',
            authDomain: 'manager-ed86e.firebaseapp.com',
            databaseURL: 'https://manager-ed86e.firebaseio.com',
            projectId: 'manager-ed86e',
            storageBucket: '',
            messagingSenderId: '515097118271'
        };
        firebase.initializeApp(config);
    }

    render() {
        return(
            <Provider store={createStore(reducers)}>
                <LoginForm />
            </Provider>
        );
    }
}

export default Core;
